from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from phonebook.users.api.views import UserViewSet
from phonebook.phonenumbers.api.views import PhoneNumberViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("phonenumbers", PhoneNumberViewSet)

app_name = "api"
urlpatterns = router.urls
