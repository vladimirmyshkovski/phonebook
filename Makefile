down:
	@sudo docker-compose -f local.yml down

up:
	@sudo docker-compose -f local.yml up

build:
	@sudo docker-compose -f local.yml build

start:
	@sudo docker-compose -f local.yml down && \
		 sudo docker-compose -f local.yml up --build

translate:
	@sudo docker-compose -f local.yml run --rm django python manage.py makemessages && \
		 sudo docker-compose -f local.yml run --rm django python manage.py compilemessages

migration:
	sudo docker-compose -f local.yml run --rm django python manage.py makemigrations && \
	sudo docker-compose -f local.yml run --rm django python manage.py migrate

test:
	@sudo docker-compose -f local.yml run --rm django pytest . -s -vv

shell:
	@sudo docker-compose -f local.yml run --rm django python manage.py shell_plus

coverage:
	@sudo docker-compose -f local.yml run --rm django coverage run -m pytest -s -vv
	@sudo docker-compose -f local.yml run --rm django coverage report
	@sudo docker-compose -f local.yml run --rm django coverage html

mypy:
	@sudo docker-compose -f local.yml run --rm django mypy phonebook

black:
	@sudo docker-compose -f local.yml run --rm django black --target-version py37 .

reset-db:
	sudo find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "*/contrib/*" -delete && \
	sudo find . -path "*/migrations/*.pyc" -path "*/migrations/__pycache__/*.pyc" -delete && \
	sudo docker-compose -f local.yml down && \
	sudo docker-compose -f local.yml run --rm django python manage.py reset_db -c && \
	sudo docker-compose -f local.yml run --rm django python manage.py makemigrations && \
	sudo docker-compose -f local.yml run --rm django python manage.py migrate

vizualization:
	docker-compose -f local.yml run --rm django python manage.py graph_models --pygraphviz -a -g -o visualized_models.svg -L en-us -e
