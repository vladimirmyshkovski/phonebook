import pytest

from phonebook.users.models import User
from phonebook.users.tests.factories import UserFactory

from phonebook.phonenumbers.models import PhoneNumber
from phonebook.phonenumbers.tests.factories import PhoneNumberFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def phone_number() -> PhoneNumber:
    return PhoneNumberFactory()
