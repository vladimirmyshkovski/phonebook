from rest_framework.serializers import ModelSerializer

from ..models import PhoneNumber
from phonenumber_field.serializerfields import PhoneNumberField


class PhoneNumberSerializer(ModelSerializer):

    phone_number = PhoneNumberField()

    class Meta:
        model = PhoneNumber
        fields = ["first_name", "last_name", "phone_number"]
