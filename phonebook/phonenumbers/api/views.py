from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin
from rest_framework.viewsets import GenericViewSet

from django_filters.rest_framework import DjangoFilterBackend

from .serializers import PhoneNumberSerializer
from ..models import PhoneNumber


class PhoneNumberViewSet(
    ListModelMixin, RetrieveModelMixin, CreateModelMixin, GenericViewSet
):
    serializer_class = PhoneNumberSerializer
    queryset = PhoneNumber.objects.all()
    lookup_field = "phone_number"
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["first_name", "last_name", "phone_number"]
    search_fields = ["first_name", "last_name", "phone_number"]
