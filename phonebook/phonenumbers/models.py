from django.db.models import CharField, Index
from django.urls import reverse

from phonenumber_field.modelfields import PhoneNumberField
from model_utils.models import SoftDeletableModel, TimeStampedModel


class PhoneNumber(SoftDeletableModel, TimeStampedModel):

    first_name = CharField(max_length=128)
    last_name = CharField(max_length=128)
    phone_number = PhoneNumberField()

    class Meta:
        indexes = [Index(fields=["first_name", "last_name", "phone_number"])]

    def __str__(self):
        return f"{self.phone_number}"

    def get_absolute_url(self):
        """Get url for phonenumber's detail view.

        Returns:
            str: URL for phonenumber detail.

        """
        return reverse(
            "api:phonenumber-detail", kwargs={"phone_number": self.phone_number}
        )
