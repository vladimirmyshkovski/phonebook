from django.contrib import admin
from .models import PhoneNumber


class PhoneNumberAdmin(admin.ModelAdmin):
    pass


admin.site.register(PhoneNumber, PhoneNumberAdmin)
