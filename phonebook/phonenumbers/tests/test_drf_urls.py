import pytest
from django.urls import resolve, reverse

from phonebook.phonenumbers.models import PhoneNumber

pytestmark = pytest.mark.django_db


def test_phone_number_detail(phone_number: PhoneNumber):
    assert (
        reverse(
            "api:phonenumber-detail", kwargs={"phone_number": phone_number.phone_number}
        )
        == f"/api/v1/phonenumbers/{phone_number.phone_number}/"
    )
    assert (
        resolve(f"/api/v1/phonenumbers/{phone_number.phone_number}/").view_name
        == "api:phonenumber-detail"
    )


def test_phone_number_list():
    assert reverse("api:phonenumber-list") == "/api/v1/phonenumbers/"
    assert resolve("/api/v1/phonenumbers/").view_name == "api:phonenumber-list"
