import pytest

from phonebook.phonenumbers.models import PhoneNumber

pytestmark = pytest.mark.django_db


def test_phone_number_get_absolute_url(phone_number: PhoneNumber):
    assert (
        phone_number.get_absolute_url()
        == f"/api/v1/phonenumbers/{phone_number.phone_number}/"
    )


def test_phone_number__str__(phone_number: PhoneNumber):
    assert phone_number.__str__() == f"{phone_number.phone_number}"
