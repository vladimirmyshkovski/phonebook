import pytest
from django.contrib.auth import get_user_model
from django.test import RequestFactory
from rest_framework.test import force_authenticate

from phonebook.phonenumbers.api.views import PhoneNumberViewSet
from phonebook.phonenumbers.models import PhoneNumber

pytestmark = pytest.mark.django_db

User = get_user_model()


class TestPhoneNumberViewSet:
    def test_get_queryset(self, phone_number: PhoneNumber, rf: RequestFactory):
        view = PhoneNumberViewSet()
        request = rf.get("/fake-url/")

        view.request = request

        assert len(view.get_queryset()) > 0

    def test_list(self, phone_number: PhoneNumber, rf: RequestFactory):
        view = PhoneNumberViewSet.as_view({"get": "list"})
        request = rf.get("/api/v1/phonenumbers/")
        view.request = request

        response = view(request)

        assert response.data == [
            {
                "first_name": phone_number.first_name,
                "last_name": phone_number.last_name,
                "phone_number": phone_number.phone_number,
            }
        ]

    def test_retrieve(self, phone_number: PhoneNumber, rf: RequestFactory):
        view = PhoneNumberViewSet.as_view({"get": "retrieve"})
        request = rf.get(f"/api/v1/phonenumbers/{phone_number.phone_number}/")

        view.request = request

        response = view(request, phone_number=phone_number.phone_number)

        assert response.data == {
            "first_name": phone_number.first_name,
            "last_name": phone_number.last_name,
            "phone_number": phone_number.phone_number,
        }

    def test_create(self, user: User, rf: RequestFactory):
        view = PhoneNumberViewSet.as_view({"post": "create"})
        request = rf.post(
            "/api/v1/phonenumbers/",
            {
                "first_name": "Fake",
                "last_name": "User",
                "phone_number": "+995579535339",
            },
        )

        request.user = user
        force_authenticate(request, user=user)

        view.request = request

        response = view(request)

        assert response.data == {
            "first_name": "Fake",
            "last_name": "User",
            "phone_number": "+995579535339",
        }
