from factory.django import DjangoModelFactory
from factory import Faker, Sequence

from ..models import PhoneNumber


class PhoneNumberFactory(DjangoModelFactory):

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    phone_number = Sequence(lambda n: "+99557953533%d" % n)

    class Meta:
        model = PhoneNumber
