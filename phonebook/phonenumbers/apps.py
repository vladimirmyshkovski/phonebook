from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PhoneNumbersConfig(AppConfig):
    name = "phonebook.phonenumbers"
    verbose_name = _("Phone numbers")
